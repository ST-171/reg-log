import './App.css';
import { useState } from 'react';
import { Button, Form, Input, Typography, message } from 'antd';
import {UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';

function Login() {

  const login = ()=> {
    message.success("Register Successful")
  }

  return (

      <Form className='loginForm' onFinish={login}>

        <Typography.Title>Login</Typography.Title>

        <Form.Item 
        rules={[
          {
            required:true,
            type:'email',
            message:'Please enter valid email'
          }
        ]} 
        name={"myEmail"} className='registerForm__item'>
          <Input prefix={<MailOutlined />} placeholder='Enter your email'/>
        </Form.Item>

        <Form.Item 
        rules={[
          {
            required:true,
            message: "Please enter your password"
          }
        ]} 
        name={"myPassword"} className='registerForm__item'>
          <Input.Password prefix={<LockOutlined />} placeholder='Enter your password'/>
        </Form.Item>

        <p className='forgot=pass'>Lost password? <a>Click here</a></p>
        <div className='btn-box'>
          <Button type='primary' htmlType='submit' >
            Sign Up
          </Button>
          <Button type='primary' htmlType='submit' >
            Login
          </Button>
        </div>
        
      </Form>
  );
}

export default Login;
