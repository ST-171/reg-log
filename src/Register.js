import './App.css';
import { useState } from 'react';
import { Button, Form, Input, Typography, message } from 'antd';
import {UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';
import PasswordChecklist from 'react-password-checklist';

function Register() {
  const [form] = Form.useForm()
  const [action, setAction] = useState("Sign Up");
  const [password, setPassword] = useState("")
  
  const onFinish = ()=>{
    message.success('Successful!');

    setTimeout(() => {
      window.location.reload(false);
    }, 800);
  };

  return (

      <Form className='registerForm' onFinish={onFinish} form={form}>

        <Typography.Title>{action}
        <div className='underline'></div>
        </Typography.Title>
        
        {action==="Login" ? <Form></Form>: <Form.Item 
        rules={[
          {
            required:true,
            message:'Please enter your name and surname'
          }
        ]} 
        name={"myName"} className='registerForm__item'>
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder='Enter your name and surname'/>
        </Form.Item>}

        <Form.Item 
        rules={[
          {
            required:true,
            type:'email',
            message:'Please enter valid email'
          }
        ]} 
        name={"myEmail"} className='registerForm__item'>
          <Input prefix={<MailOutlined />} placeholder='Enter your email'/>
        </Form.Item>

        <Form.Item 
        rules={[
          {
            required:true,
            message: "Please enter your password"
          }
        ]} 
        name={"myPassword"} className='registerForm__item'>
          <Input.Password prefix={<LockOutlined />} placeholder='Enter your password' onChange={e => setPassword(e.target.value)} />
        </Form.Item>

        {action==="Sign Up" ? <PasswordChecklist
				rules={["minLength","specialChar", "maxLength"]}
				minLength={8}
				maxLength={24}
				value={password}
			/>:<span></span>}
        

        {action==="Login" ? <p className='forgot=pass'>Lost password? <a>Click here</a></p>:<p></p>}

        <div className='btn-box'>
          <Button type='primary' htmlType='submit' className={action==="Login" ? "gray" : ""} onClick={() => {setAction("Sign Up")}}>
            Sign Up
          </Button>
          <Button type='primary' htmlType='submit' className={action==="Sign Up" ? "gray" : ""} onClick={() => {setAction("Login")}}>
            Login
          </Button>
        </div>
        
      </Form>
  );
}

export default Register;
