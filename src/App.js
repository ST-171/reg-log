import './App.css';
import { useState } from 'react';
import { Button, Form, Input, Typography, message } from 'antd';
import Register from './Register'

function App() {
  return (
    <div className="appBg">
      <Register />
    </div>
  );
}

export default App;
